//
//  NotificationController.h
//  klipper WatchKit Extension
//
//  Created by dev on 2015/04/17.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
