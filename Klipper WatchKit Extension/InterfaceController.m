//
//  InterfaceController.m
//  klipper WatchKit Extension
//
//  Created by dev on 2015/04/17.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//

#import "InterfaceController.h"

@interface InterfaceController()

- (IBAction)reloadButton;
- (IBAction)getClipboardButton;

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *textArea;

@end


@implementation InterfaceController

@synthesize textArea;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    //ユーザデフォルトから設定値を読み込む
    userDefaultForGroup = [[NSUserDefaults alloc]initWithSuiteName:@"group.com.fujiinfotech.klipper"];
    
    NSString *str = [userDefaultForGroup objectForKey:@"key_sentNote"];
    
    if (nil != str && ![@"" isEqualToString:str]) {
        [textArea setText:str];
    }
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (IBAction)reloadButton {
    
    //ユーザデフォルトから設定値を読み込む
    userDefaultForGroup = [[NSUserDefaults alloc]initWithSuiteName:@"group.com.fujiinfotech.klipper"];
    
    NSString *str = [userDefaultForGroup objectForKey:@"key_sentNote"];
    
    if (nil != str && ![@"" isEqualToString:str]) {
        [textArea setText:str];
    }
    
    //NSLog(@"str: %@", str);
    
}

- (IBAction)getClipboardButton {
    
    //リマインダーにアクセスできるとき
    if([EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder] == EKAuthorizationStatusAuthorized) {
        //リマインダ取り出し
        EKEventStore *rStore = [[EKEventStore alloc] init];
        NSPredicate *predicate = [rStore predicateForIncompleteRemindersWithDueDateStarting:nil ending:nil calendars:nil];
        
        [rStore fetchRemindersMatchingPredicate:predicate completion:^(NSArray *reminders)
         {
             NSMutableString* str = [NSMutableString string];
             
             for (EKReminder *reminder in reminders) {
                 // 各リマインダーについての処理
                 if (nil != reminder.title) {
                     
                     //NSLog(@"title=%@", reminder.title);
                     [str appendString:@"・"];
                     [str appendString:reminder.title];
                     [str appendString:@"\n"];
                 }
             }
             
             [textArea setText:str];
         }];
        
        //アクセスできないとき
    } else {
        
        [textArea setText:NSLocalizedString(@"no_permission", nil) ];
    }
    
}
@end



