//
//  GlanceController.m
//  klipper WatchKit Extension
//
//  Created by dev on 2015/04/17.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//

#import "GlanceController.h"


@interface GlanceController()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *mainTextLabel1;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *dateLabel;

@end


@implementation GlanceController

@synthesize mainTextLabel1;
@synthesize dateLabel;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    //ユーザデフォルトから設定値を読み込む
    userDefaultForGroup = [[NSUserDefaults alloc]initWithSuiteName:@"group.com.fujiinfotech.klipper"];
    
    NSString *strNote = [userDefaultForGroup objectForKey:@"key_sentNote"];
    
        NSString *strFireDate = [userDefaultForGroup objectForKey:@"key_fireDate"];
    
    if (nil != strNote && ![@"" isEqualToString:strNote]) {
        [mainTextLabel1 setText:strNote];
    }
    
    if (nil != strFireDate && ![@"" isEqualToString:strFireDate]) {
        [dateLabel setText:strFireDate];
    }
/*
    //test
    [mainTextLabel1 setText:@"多くの乗客の命を預かる航空機パイロットは、法定の航空身体検査の項目で１つでも不合格となれば乗務できない。年間１万人余りが申請するが、厳しい内容のため、不合格者は１千人以上に及ぶ。航空需要の高まりに伴うパイロット不足が今後深刻化する見通しだが、質と量を両立させる抜本的な解決策は見いだせていない。"];
*/
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



