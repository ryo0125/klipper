//
//  InterfaceController.h
//  klipper WatchKit Extension
//
//  Created by dev on 2015/04/17.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>


#import <EventKit/EventKit.h>

//#import <EventKit/EventKit.h>

@interface InterfaceController : WKInterfaceController
{
    NSUserDefaults *userDefaultForGroup;
}
@end

/*
@interface MyEventManager : NSObject

@property (nonatomic, retain) EKEventStore *sharedEventKitStore;
+ (MyEventManager *)sharedInstance;

@end

*/