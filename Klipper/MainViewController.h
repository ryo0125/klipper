//
//  MainViewController.h
//  Memo from picture
//
//  Created by dev on 2014/05/21.
//  Copyright (c) 2014年 fujiinfotech.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <EventKit/EventKit.h>

@interface MainViewController : UIViewController{

    UIAlertView *globalAlert;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UITextField *inputField;

@property (strong, nonatomic) UIAlertView *globalAlert;

//@property (strong, nonatomic) EKEventStore *eventStore;

@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (weak, nonatomic) IBOutlet UITextView *lastNoteTextView;

- (IBAction)memoField:(UITextField *)sender;

- (IBAction)choosePhotoButton:(id)sender;

- (IBAction)getClipboardTextButton:(id *)sender;

- (IBAction)sendReminderButton:(id)sender;

- (IBAction)getLastNote:(id)sender;

+ (NSString *)dateToString:(NSDate *)baseDate formatString:(NSString *)formatString;

@end