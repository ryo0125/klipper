//
//  main.m
//  Klipper
//
//  Created by dev on 2015/03/26.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
