//
//  AppDelegate.h
//  Klipper
//
//  Created by dev on 2015/03/26.
//  Copyright (c) 2015年 FUJIIT. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    BOOL urlOpened;
    UIImage *uiImage;
}

@property (strong, nonatomic) UIWindow *window;

@property BOOL urlOpened;
@property UIImage *uiImage;

@end
