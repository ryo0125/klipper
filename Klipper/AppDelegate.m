//
//  AppDelegate.m
//  Klipper
//
//  Created by dev on 2014/05/21.
//  Copyright (c) 2014年 fujiinfotech.com. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@interface AppDelegate ()
@property MainViewController *mainViewController;

@end

@implementation AppDelegate

@synthesize urlOpened;
@synthesize uiImage;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    
    //NSLog(@"%@ = localNotif/didFinishLaunchingWithOptions", localNotif);
    
    if (localNotif == NULL) {
        
        urlOpened = NO;
    }else{
        
        urlOpened = YES;
    }

    
    UIUserNotificationType types =
    UIUserNotificationTypeBadge|
    UIUserNotificationTypeSound|
    UIUserNotificationTypeAlert;
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [application registerUserNotificationSettings:mySettings];
    
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    //NSLog(@"openURL: in  url.path/openURL = %@ ", url.path);
    
    if ([url.scheme isEqualToString:@"file"]) {
        
        uiImage = [UIImage imageWithContentsOfFile:url.path];
        
        //画像をセット
        [self.mainViewController.imageView setImage:uiImage];
        
        //ズームを戻す
        [self.mainViewController.scrollView setZoomScale:1.0 animated:YES];
        
        return YES;
        
    } else if ([url.scheme isEqualToString:@"klipper"]) {
        // カスタムURLスキームの処理
    }
    
    return YES;
}

//ホームボタン押下
- (void)applicationWillResignActive:(UIApplication *)application
{
    self.mainViewController = (MainViewController *)self.window.rootViewController;
    
    if (self.mainViewController.globalAlert != nil && self.mainViewController.globalAlert.isVisible){
        
        //        NSLog(@"%d self.mainViewController.globalAlert.numberOfButtons", self.mainViewController.globalAlert.numberOfButtons);
        
        [self.mainViewController.globalAlert dismissWithClickedButtonIndex:0 animated:NO];
    }
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
