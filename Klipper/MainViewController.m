//
//  MainViewController.m
//  Memo from picture
//
//  Created by dev on 2014/05/21.
//  Copyright (c) 2014年 fujiinfotech.com. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"


@interface MainViewController () <

UIImagePickerControllerDelegate, UIScrollViewDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *tempImageView;

@property BOOL focusOnInputField;

@property NSString* resultText;

@property UIView* snapView;

@property CGRect windowCGRect;

@property (strong, nonatomic) UIDocumentInteractionController *docInteractionController;

@end

@implementation MainViewController

//@synthesize eventStore;
@synthesize globalAlert;
@synthesize imagePicker;

@synthesize scrollView;
@synthesize imageView;
@synthesize focusOnInputField;
@synthesize resultText;

@synthesize lastNoteTextView;

@synthesize tempImageView;
@synthesize snapView;

@synthesize windowCGRect;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //画像の拡大を可能に設定
    scrollView.minimumZoomScale = 1.0;
    scrollView.maximumZoomScale = 12.0;
    scrollView.delegate = self;
    
    //scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // ダブルタップジェスチャーを作る
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    
    // ダブルタップを認識する設定にする
    doubleTapGesture.numberOfTapsRequired = 2;
    
    // イメージビューにダブルタップジェスチャーを設定する
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:doubleTapGesture];
    
    
    //ユーザデフォルトから設定値を読み込む
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *str = [userDefault objectForKey:@"key_lastNote"];
    
    if (nil != str && ![@"" isEqualToString:str]) {
        [lastNoteTextView setText:str];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"%@ imageView.image/viewDidAppear", imageView.image);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    //urlOpenでない時は画像選択画面を自動表示
    if(!appDelegate.urlOpened){
        
        //起動時のカメラロールオープンを無しに変更
        //[self openCameraRoll];
        //appDelegate.urlOpened = YES;
    
    //urlOpen時は画像を設定 (起動後もここに入るのでimageがないときに限定)
    }else{
        
        if (appDelegate.uiImage != nil && self.imageView.image == nil) {

            [self.imageView setImage:appDelegate.uiImage];
        }
    }
    
    [super viewDidAppear:animated];
}

//ダブルタップで拡大/縮小
- (void)doubleTap:(UIGestureRecognizer *)gesture {
    //最大倍率でなければ拡大
    
    if(scrollView.zoomScale < scrollView.maximumZoomScale)
    {
        //現在の3倍の倍率に
        float newScale = scrollView.zoomScale * 3;
        
        //拡大する領域を取得
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gesture locationInView:gesture.view]];
            
        //タップした位置を拡大
        [scrollView zoomToRect:zoomRect animated:YES];
        
        //余白を追加
        //scrollView.contentInset = UIEdgeInsetsMake(100.0, 100.0, 100.0, 100.0);
        
    } else {
        
        //ズームを戻し、余白を削除
        [scrollView setZoomScale:1.0 animated:YES];
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

// 指定の座標を中心にして拡大する領域を取得
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    CGRect zoomRect;
    
    // 倍率から拡大する縦横サイズを取得
    zoomRect.size.height = [scrollView frame].size.height / scale;
    zoomRect.size.width  = [scrollView frame].size.width / scale;
    
    // 座標を設定
    zoomRect.origin.x = center.x - (zoomRect.size.width/2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height/2.0);
    
    return zoomRect;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

//写真選択終了時の処理
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [imageView setImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //ズームを戻し、余白を削除
    [scrollView setZoomScale:1.0 animated:YES];
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

//写真選択キャンセル時の処理
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{

    //キャンセルのみ行ったとき画像が小さくなる問題への対応
    [scrollView setZoomScale:1.0 animated:YES];
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}
*/

//写真選択ボタンが押されたとき
-(void)choosePhotoButton:(id)sender{
    
    [tempImageView setImage:nil];
    
    [self openCameraRoll];
}

- (IBAction)getClipboardTextButton:(id *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
     
    NSString *str = [pasteboard valueForPasteboardType:@"public.text"];
    
    //trim
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (nil != str && ![@"" isEqualToString:str]) {
        
        //NSLog( @"%@",pasteboard.pasteboardTypes);
        
        self.inputField.text = str;
        
        [self.inputField becomeFirstResponder];
    }
}

- (IBAction)sendReminderButton:(id)sender {
    
    if([EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder] == EKAuthorizationStatusAuthorized) {
        // リマインダーにアクセスできる場合
        
        [self sendReminderContents];
        
    } else {
        EKEventStore *eStore = [[EKEventStore alloc] init];
        [eStore requestAccessToEntityType:EKEntityTypeReminder
                                   completion:^(BOOL granted, NSError *error) {
                                       if(granted) {
                                           // はじめてリマインダーにアクセスする場合にアラートが表示されて、OKした場合にここにくる
                                           
                                           [self sendReminderContents];
                                           
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // アクセス権がありません。
                                               // "プライバシー設定"でアクセスを有効にできます。
                                               UIAlertView *alert = [[UIAlertView alloc]
                                                                     initWithTitle:NSLocalizedString(@"This app does not have access to your reminders.", nil)
                                                                     message:NSLocalizedString(@"To display your reminder, enable Klipper in the \"Privacy\" → \"Reminders\" in the Settings", nil)
                                                                     delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                     otherButtonTitles:nil];
                                               [alert show];
                                           });
                                       }
                                   }
         ];
    }
}

- (IBAction)getLastNote:(id)sender {
    
    if (lastNoteTextView.hasText) {
        
        NSString *str = lastNoteTextView.text;
        
        //trim
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (nil != str && ![@"" isEqualToString:str]) {

            self.inputField.text = str;
        
            [self.inputField becomeFirstResponder];
        }
    }
}

//リマインダーの項目を送信
-(void)sendReminderContents{

    EKEventStore *rStore = [[EKEventStore alloc] init];
    
    //リマインダ取り出し
    NSPredicate *predicate = [rStore predicateForIncompleteRemindersWithDueDateStarting:nil ending:nil calendars:nil];
    
    NSUserDefaults *userDefaultForGroup = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.fujiinfotech.klipper"];
    
    //__block NSMutableString *str = [NSMutableString string];
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [rStore fetchRemindersMatchingPredicate:predicate completion:^(NSArray *reminders)
    {
        NSMutableString *str = [NSMutableString string];
        
        for (EKReminder *reminder in reminders) {
            // 各リマインダーについての処理
            if (nil != reminder.title) {
                
                //NSLog(@"title=%@", reminder.title);
                [str appendString:@""];
                [str appendString:reminder.title];
                [str appendString:@"/ "];
            }
        }
        
        //履歴 には残さない
        //[userDefault setObject:str forKey:@"key_lastNote"];
        
        [userDefaultForGroup setObject:str forKey:@"key_sentNote"];
        //現在の時刻文字列
        NSString *dateStr = [MainViewController dateToString:[NSDate date] formatString:@"MM/dd HH:mm"];
        [userDefaultForGroup setObject:dateStr forKey:@"key_fireDate"];
        
        //明示的に保存
        //[userDefault synchronize];
        [userDefaultForGroup synchronize];
        
        //通知（ペブル用）
        UILocalNotification *noti = [[UILocalNotification alloc] init];
        noti.fireDate = [NSDate date];
        noti.timeZone = [NSTimeZone defaultTimeZone];
        noti.alertTitle = str;
        noti.alertBody = str;
        noti.alertAction = @"OPEN";
        noti.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:noti];
        
    }];
    
    //テキストフィールドに設定
    //ブロック処理後に処理するようにしないと無理
    //self.lastNoteTextView.text = [userDefault objectForKey:@"key_lastNote"];
    
    //ダイアログ表示
    NSString *title = @"Sent as a Notification";
    NSString *message = @"If you cannot receive, check notification setting.";
    
    globalAlert = [[UIAlertView alloc]
                   initWithTitle:NSLocalizedString(title, nil)
                   message:NSLocalizedString(message, nil)
                   delegate:self
                   cancelButtonTitle:NSLocalizedString(@"Open in", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    globalAlert.tag = 1;
    
    [globalAlert show];
}

-(void)openCameraRoll{

    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];//ソースに写真ライブラリを指定
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)memoField:(UITextField *)sender {
    
    NSString *str = self.inputField.text;
    
    //trim
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    //インプット欄がnilか空文字でなければ
    if (nil != str && ![@"" isEqualToString:str]) {
        
        //メモ内容
        NSString *noteStr = str;
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate date];
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.alertTitle = noteStr;
        notification.alertBody = noteStr;
        notification.alertAction = @"OPEN";
        
        notification.soundName = UILocalNotificationDefaultSoundName;
        
        // 通知登録
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        //ユーザーデフォルトに保存
        //履歴用
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:noteStr forKey:@"key_lastNote"];
        
        //ウォッチ共有用
        NSUserDefaults *userDefaultForGroup = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.fujiinfotech.klipper"];
        //メモ
        [userDefaultForGroup setObject:noteStr forKey:@"key_sentNote"];
        //現在の時刻文字列
        NSString *dateStr = [MainViewController dateToString:[NSDate date] formatString:@"MM/dd HH:mm"];
        [userDefaultForGroup setObject:dateStr forKey:@"key_fireDate"];
         
        //明示的に保存
        [userDefault synchronize];
        [userDefaultForGroup synchronize];
        
        //テキストフィールドに設定
        [lastNoteTextView setText:noteStr];
        
        NSString *title = @"Sent as a Notification";
        NSString *message = @"If you cannot receive, check notification setting.";
        
        globalAlert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(title, nil)
                              message:NSLocalizedString(message, nil)
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"Open in", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        globalAlert.tag = 1;
        
        [globalAlert show];
    }
}

//Timer終了でアラートを閉じる
-(void)performDismiss:(NSTimer*)timer
{
    UIAlertView *alt = [timer userInfo];
    [alt dismissWithClickedButtonIndex:0 animated:NO];
}

//ダイアログ
- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
            
        //アプリ連携が押されたとき
        case 0:
            [self openApplication:self.inputField.text
             ];
             
            break;
            
        //OKが押されたとき
        case 1:
            
            //NSLog(@"case 1");
            break;
    }
}

- (void)openApplication:(NSString *)memo
{
    NSArray* actItems = [NSArray arrayWithObjects:memo, nil];

    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:actItems applicationActivities:nil];

    [self presentViewController:activityView animated:YES completion:^{}];
}

- (void)openApplication:(NSString *)memo :(UIImage *)image
{
    NSArray* actItems = [NSArray arrayWithObjects: memo, image, nil];
    
    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:actItems applicationActivities:nil];
    
    [self presentViewController:activityView animated:YES completion:^{}];
}

+ (NSString*)dateToString:(NSDate *)baseDate formatString:(NSString *)formatString
{
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    //24時間表示 & iPhoneの現在の設定に合わせる
    [inputDateFormatter setLocale:[NSLocale currentLocale]];
    [inputDateFormatter setDateFormat:formatString];
    NSString *str = [inputDateFormatter stringFromDate:baseDate];
    return str;
}

@end
